package ZK;

import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import java.util.concurrent.TimeUnit;
import static org.testng.internal.EclipseInterface.ASSERT_LEFT;
import static org.testng.internal.EclipseInterface.ASSERT_LEFT2;
import static org.testng.internal.EclipseInterface.ASSERT_MIDDLE;
import static org.testng.internal.EclipseInterface.ASSERT_RIGHT;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;

public class ZkLoginLogoutTest {
	@Test		
       public void verifyTitle() {
	        System.setProperty("webdriver.chrome.driver", "D:\\drivers\\chromedriver.exe");
			WebDriver driver =new ChromeDriver();
			driver.get("http://demo.libreplan.org/libreplan/common/layout/login.zul");
	        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            WebElement userFeild = driver.findElement(By.id("textfield"));
            userFeild.clear();
            userFeild.sendKeys("admin");
            WebElement passFeild = driver.findElement(By.id("textfield2"));
            passFeild.clear();
            passFeild.sendKeys("admin");
            WebElement loginButoon = driver.findElement(By.id("button"));
            loginButoon.click();
	        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            String title=driver.getTitle();
            AssertJUnit.assertEquals("LibrePlan: Planning",title);  
            driver.close();
            }

	@Test		
	public void verifyTitle2() {
		System.setProperty("webdriver.chrome.driver", "D:\\drivers\\chromedriver.exe");
		WebDriver driver =new ChromeDriver();
		driver.get("http://demo.libreplan.org/libreplan/common/layout/login.zul");
	     System.setProperty("webdriver.chrome.driver", "D:\\drivers\\chromedriver.exe");
	        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            WebElement userFeild = driver.findElement(By.id("textfield"));
            userFeild.clear();
            userFeild.sendKeys("admin");
            WebElement passFeild = driver.findElement(By.id("textfield2"));
            passFeild.clear();
            passFeild.sendKeys("admin");
            WebElement loginButoon = driver.findElement(By.id("button"));
            loginButoon.click();
	        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            String title=driver.getTitle();
            System.out.println(title);
            AssertJUnit.assertEquals("LibrePlan: Planning",title);  
            driver.findElement(By.linkText("[Log out]")).click();	
	        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	        title=driver.getTitle();
		     System.out.println(title);
	        AssertJUnit.assertEquals("LibrePlan: User access",title);  
	        driver.close();
	       
	
	}
	@Test
	public void verifyTitle3() {
        System.setProperty("webdriver.chrome.driver", "D:\\drivers\\chromedriver.exe");
		WebDriver driver =new ChromeDriver();
		driver.get("http://demo.libreplan.org/libreplan/common/layout/login.zul");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebElement userFeild = driver.findElement(By.id("textfield"));
        userFeild.clear();
        userFeild.sendKeys("admin");
        WebElement passFeild = driver.findElement(By.id("textfield2"));
        passFeild.clear();
        passFeild.sendKeys("admi");
        WebElement loginButoon = driver.findElement(By.id("button"));
        loginButoon.click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        String title=driver.getTitle();
        AssertJUnit.assertEquals("LibrePlan: Planning",title);  
        driver.close();
        }
            
            

	}



