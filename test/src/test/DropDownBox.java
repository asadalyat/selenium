package test;

import java.util.Collection;
import java.util.List;

import org.apache.bcel.generic.Select;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.google.common.base.Supplier;
import com.google.common.collect.Lists;

public class DropDownBox {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	    System.setProperty("webdriver.chrome.driver", "D:\\drivers\\chromedriver.exe");
			WebDriver driver=new ChromeDriver();
			driver.get("https://eservices.iqrad.edu.ps/StuReg");
			
			WebElement dropDown  = driver.findElement(By.xpath("//*[@id=\"ctl00_ContentPlaceHolder1_ctl00_cmbEduOrgs\"]"));
		org.openqa.selenium.support.ui.Select dropBox=new org.openqa.selenium.support.ui.Select(dropDown);
         dropBox.selectByIndex(5);
         
         List<WebElement> count= dropBox.getOptions();
         int number = count.size();
         System.out.println("number is "+number);
         
         
	}

}
