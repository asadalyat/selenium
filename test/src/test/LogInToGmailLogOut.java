package test;


import java.util.Collection;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.google.common.base.Supplier;
import com.google.common.collect.Lists;


public class LogInToGmailLogOut {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
        System.setProperty("webdriver.chrome.driver", "D:\\drivers\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		// open log in page 
		driver.get("https://accounts.google.com/signin");
		// fill user name in user name field 
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//*[@id=\"identifierId\"]")).sendKeys("alyatasad@gmail.com");
		// click next button
		driver.findElement(By.xpath("//*[@id=\"identifierNext\"]/content/span")).click();
		// enter the password in password field 
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//*[@id=\"password\"]/div[1]/div/div[1]/input")).sendKeys("000");
		// click log in button
		driver.findElement(By.xpath("//*[@id=\"passwordNext\"]/content")).click();
		//wait max 10 seconds second after click log in button 
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        // click on google applications button
		driver.findElement(By.xpath("//*[@id=\"gbwa\"]/div[1]/a")).click();
		// click on gmail option
		driver.findElement(By.xpath("//*[@id=\"gb23\"]/span[2]")).click();//*[@id="gb"]/div[1]/div[1]/div[2]/div[4]/div[1]/a
		// click on the picture of acounts owner
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//*[@id=\"gb\"]/div[1]/div[1]/div[2]/div[4]/div[1]/a/span")).click();
		//click sign up button
		driver.findElement(By.xpath("//*[@id=\"gb_71\"]")).click();
		
	}

}
