
// Using different elements locators 
package test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class MappingBrowserElements {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		
		
		
		System.setProperty("webdriver.chrome.driver", "D:\\drivers\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		// open this site
		driver.get("https://www.google.com");
		// wait to start the browser 
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        // use class name locator
        driver.findElement(By.className("gsfi")).sendKeys("yahoo");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        // use name locator
        driver.findElement(By.name("btnK")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        // click on the first result using xpath locator 
        driver.findElement(By.xpath("//*[@id=\"rso\"]/div[1]/div/div/div/div/h3/a/span")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        // click on the back button
        driver.navigate().back();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        //click on the back button
        driver.navigate().back();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        // open wikipedia
		driver.get("https://ar.wikipedia.org");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        // click on the create account 
        driver.findElement(By.id("pt-createaccount")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        // click on the wikipedia logo 
        driver.findElement(By.className("mw-wiki-logo")).click();
        driver.navigate().back();
        driver.navigate().back();
        driver.navigate().back();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        //click on the Gmail text link using  link text 
        driver.findElement(By.linkText("Gmail")).click();
        




        


        
        
		
	}

}
