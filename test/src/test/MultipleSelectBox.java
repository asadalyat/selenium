package test;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.bcel.generic.Select;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Sleeper;

public class MultipleSelectBox {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "D:\\drivers\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		// open this site
		driver.get("https://select2.github.io/examples.html");
		// delay to open the site 
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		// find multiple selection box using css selctor 
		WebElement Multiple =driver.findElement(By.cssSelector("#id_label_multiple"));
		
		org.openqa.selenium.support.ui.Select selection = new org.openqa.selenium.support.ui.Select(Multiple);
      
		List<WebElement> count= selection.getOptions();
         int number = count.size();
         System.out.println("number is "+number);
         for(int i=0;i<number;i++)
         {
        	 //select by index
     	  selection.selectByIndex(i);
 
         }
         // wait 5 seconds 
         Thread.sleep(5000);
         // deselect all items 
         selection.deselectAll();
         // wait 5 seconds 
         Thread.sleep(5000);
         // select by visible text 
         selection.selectByVisibleText("Colorado");
        
 

         
		


	    
	
	}
	

}
